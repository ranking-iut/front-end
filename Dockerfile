# Building build.js with a temporary Node container
FROM node:carbon-alpine as build
WORKDIR /app
COPY package.json .
COPY webpack.config.js .
# .babelrc needed by UglifyJS (accepting only ES5 code)
COPY .babelrc .
COPY src ./src
RUN npm install
RUN npm run build

# Building the final container with the dist folder from the temporary build container
FROM nginx:1.13-alpine
COPY --from=build /app/dist /usr/share/nginx/html/dist
COPY index.html /usr/share/nginx/html
COPY public /usr/share/nginx/html/public
