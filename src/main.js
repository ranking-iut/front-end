import Vue from 'vue'
import Router from 'vue-router'
import App from './components/App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import Routes from './config/routes'

Vue.use(Vuetify)
Vue.use(Router)

const router = new Router({
	mode: 'history',
	routes: Routes
})

new Vue({
	el: '#app',
	router: router,
	render: h => h(App)
})
