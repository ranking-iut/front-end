import Login from "../components/Login.vue";
import Register from "../components/Register.vue";

export default [
	{
		path: '/',
		redirect: '/login'
	},
	{
		path: '/login',
		component: Login
	},
	{
		path: '/register',
		component: Register
	}
]